/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author theodofilius
 */
import app.model.Dokter;
import java.util.List;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;

public class DataLinkDokterImpl implements DataLinkDokter{

    Connection connection = null;
    Statement statement = null;
    ResultSet resultset = null;
    PreparedStatement pre_statement = null;
    final String select = "SELECT * FROM dokter";
    final String update = "UPDATE dokter set DokterID=?, nama=?, spesialis=?;";
    final String insert = "INSERT INTO dokter (DokterID, nama, spesialis) VALUES (?,?,?);";
    final String delete = "DELETE from dokter where DokterID=?;";

//    Constructor
    public DataLinkDokterImpl() {
        LoadDriverJDBC driver = new LoadDriverJDBC();
        driver.loadDriver();
    }
    
    @Override
    public List<Dokter> getAllDokter() {
        List<Dokter> list_dokter = null;
        try {
            statement = connection.createStatement();
            resultset = statement.executeQuery(select);
            while (resultset.next()) {
                Dokter dokter = new Dokter();
                dokter.setID(resultset.getInt("DokterID"));
                dokter.setNamaD(resultset.getString("nama"));
                dokter.setSpesialisD(resultset.getString("spesialis"));
                list_dokter.add(dokter);
            }
            if (resultset != null) {
                resultset.close();
                statement.close();
                System.out.println("Query MySQL pada tabel Dokter berhasil dilakukan oleh Client");
            } else {
                System.out.println("Query SQL gagal dilakukan oleh Client");
            }
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
        return list_dokter;
    }
    @Override
    public void insertDokter(Dokter dokter) {
        try {
            pre_statement = connection.prepareStatement(insert);
            pre_statement.setInt(1, dokter.getID());
            pre_statement.setString(2, dokter.getNamaD());
            pre_statement.setString(3, dokter.getSpesialisD());
            System.out.println("Client berhasil melakukan insert pada tabel Dokter");
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }

    }
    @Override
    public void updateDokter(Dokter dokter) {
        try {
            pre_statement = connection.prepareStatement(insert);
            pre_statement.setInt(1, dokter.getID());
            pre_statement.setString(2, dokter.getNamaD());
            pre_statement.setString(3, dokter.getSpesialisD());
            System.out.println("Client berhasil melakukan update pada tabel Dokter");
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
    }
    @Override
    public void deleteDokter(Dokter dokter) {
        try {
            pre_statement = connection.prepareStatement(delete);
            pre_statement.setInt(1, dokter.getID());
            System.out.println("Client berhasil melakukan delete pada tabel Dokter");
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
    }
}
