/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import app.model.Pasien;
import java.util.List;

import java.util.ArrayList;

/**
 *
 * @author theodofilius
 */
public interface DataLinkPasien {

    public List<Pasien> getAllPasien();

    public void insertPasien(Pasien pasien);

    public void updatePasien(Pasien pasien);

    public void deletePasien(Pasien pasien);
}
