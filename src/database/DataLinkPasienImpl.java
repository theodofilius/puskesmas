/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import app.model.Pasien;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

/**
 *
 * @author theodofilius
 */
public class DataLinkPasienImpl implements DataLinkPasien {

    Connection connection = null;
    ResultSet resultset = null;
    Statement statement = null;
    PreparedStatement pre_statement = null;

    final String select = "SELECT * FROM pasien";
    final String insert = "INSERT INTO pasien (paseinID, nama, umur, tgl_lahir, alamat) VALUES (?,?,?,?,?);";
    final String update = "UPDATE pasien set pasienID=?, nama=?, tgl_lahir=?, alamat=?";
    final String delete = "DELETE FROM pasien WHERE id=?;";
//Constructor

    public DataLinkPasienImpl() {
        //Connection
        LoadDriverJDBC driver = new LoadDriverJDBC();
        connection = driver.loadDriver();
    }
//    Akhir Constructor

    @Override
    public List<Pasien> getAllPasien() {
        List<Pasien> list_pasien = null;
        try {
            statement = connection.createStatement();
            resultset = statement.executeQuery(select);
            while (resultset.next()) {
                Pasien pasien = new Pasien();
                pasien.setID(resultset.getInt("pasienID"));
                pasien.setNama(resultset.getString("name"));
                pasien.setUmur(resultset.getInt("umur"));
                pasien.setTgl(resultset.getString("tgl_lahir"));
                pasien.setAlamat(resultset.getString("alamat"));
                list_pasien.add(pasien);
            }
            if (resultset != null) {
                resultset.close();
                statement.close();
                System.out.println("Query MySQL pada tabel Pasien berhasil dilakukan oleh Client");
            } else {
                System.out.println("Query SQL gagal dilakukan oleh Client");
            }
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
        return list_pasien;
    }

    @Override
    public void insertPasien(Pasien pasien) {
        try {
            pre_statement = connection.prepareStatement(insert);
//            Inserting table pasien
            pre_statement.setInt(1, pasien.getID());
            pre_statement.setString(2, pasien.getNama());
            pre_statement.setInt(3, pasien.getUmur());
            pre_statement.setString(4, pasien.getTgl());
            pre_statement.setString(5, pasien.getAlamat());
            System.out.println("Client berhasil melakukan insert pada tabel Pasien");
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
    }

    @Override
    public void updatePasien(Pasien pasien) {
        try {
            pre_statement = connection.prepareStatement(insert);
//            updating table pasien
            pre_statement.setInt(1, pasien.getID());
            pre_statement.setString(2, pasien.getNama());
            pre_statement.setInt(3, pasien.getUmur());
            pre_statement.setString(4, pasien.getTgl());
            pre_statement.setString(5, pasien.getAlamat());
            System.out.println("Client berhasil melakukan update pada tabel Pasien");
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
    }

    @Override
    public void deletePasien(Pasien pasien) {
        try {
            pre_statement = connection.prepareStatement(delete);
//        deleting coloum table pasien
            pre_statement.setInt(1, pasien.getID());
            System.out.println("Client berhasil melakukan delete pada tabel Pasien");
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
    }
}
