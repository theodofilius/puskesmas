/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.util.List;
import app.model.Farmasi;

/**
 *
 * @author theodofilius
 */
public interface DataLinkFarmasi {

    List<Farmasi> getAllFarmasi();

    public void insertFarmasi(Farmasi farmasi);

    public void updateFarmasi(Farmasi farmasi);

    public void deleteFarmasi(Farmasi farmasi);
}
