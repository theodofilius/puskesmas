/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import app.model.Dokter;
import java.util.List;
import app.model.Farmasi;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author theodofilius
 */
public class DataLinkFarmasiImpl implements DataLinkFarmasi {

    Connection connection = null;
    Statement statement = null;
    ResultSet resultset = null;
    PreparedStatement pre_statement = null;
    final String select = "SELECT * FROM farmasi";
    final String update = "UPDATE farmasi set ObatID=?, DokterID=?, PasienID=?, LabID=?, NamaObat=?, Pemakaian=?;";
    final String insert = "INSERT INTO farmasi (ObatID, DokterID, PasienID, LabID, NamaObat, Pemakaian) VALUES (?,?,?,?,?,?);";
    final String delete = "DELETE from farmasi where ObatID=?;";
//    Constructor

    public DataLinkFarmasiImpl() {
        LoadDriverJDBC driver = new LoadDriverJDBC();
    }

    @Override
    public List<Farmasi> getAllFarmasi() {
        List<Farmasi> list_farmasi = null;
        try {
            statement = connection.createStatement();
            resultset = statement.executeQuery(select);
            while (resultset.next()) {
                Farmasi farmasi = new Farmasi();
                farmasi.setObatId(resultset.getInt("ObatID"));
                farmasi.setDokterId(resultset.getInt("ObatID"));
                farmasi.setPasienId(resultset.getInt("ObatID"));
                farmasi.setLabId(resultset.getInt("ObatID"));
                farmasi.setNamaObat(resultset.getString("NamaObat"));
                farmasi.setPemakaian(resultset.getString("Pemakaian"));
                list_farmasi.add(farmasi);
            }
            if (resultset != null) {
                resultset.close();
                statement.close();
                System.out.println("Query MySQL pada tabel Farmasi berhasil dilakukan oleh Client");
            } else {
                System.out.println("Query SQL gagal dilakukan oleh Client");
            }
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
        return list_farmasi;
    }

    @Override
    public void insertFarmasi(Farmasi farmasi) {
        try {
            pre_statement = connection.prepareStatement(insert);
            pre_statement.setInt(1, farmasi.getObatId());
            pre_statement.setInt(2, farmasi.getDokterId());
            pre_statement.setInt(3, farmasi.getPasienId());
            pre_statement.setInt(4, farmasi.getLabId());            
            pre_statement.setString(5, farmasi.getNamaObat());
            pre_statement.setString(6, farmasi.getPemakaian());
            System.out.println("Client berhasil melakukan insert pada tabel Farmasi");
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
    }

    @Override
    public void updateFarmasi(Farmasi farmasi) {
        try {
            pre_statement = connection.prepareStatement(insert);
            pre_statement.setInt(1, farmasi.getObatId());
            pre_statement.setInt(2, farmasi.getDokterId());
            pre_statement.setInt(3, farmasi.getPasienId());
            pre_statement.setInt(4, farmasi.getLabId());            
            pre_statement.setString(5, farmasi.getNamaObat());
            pre_statement.setString(6, farmasi.getPemakaian());
            System.out.println("Client berhasil melakukan update pada tabel Farmasi");
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
    }

    @Override
    public void deleteFarmasi(Farmasi farmasi) {
        try {
            pre_statement = connection.prepareStatement(delete);
            pre_statement.setInt(1, farmasi.getObatId());
            System.out.println("Client berhasil melakukan update pada tabel Farmasi");
        } catch (SQLException e) {
            System.out.println("Gagal memasukkan perintah");
            e.printStackTrace();
        }
    }

}
