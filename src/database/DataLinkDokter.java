/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import app.model.Dokter;
import java.util.List;

/**
 *
 * @author theodofilius
 */
public interface DataLinkDokter {
    public List<Dokter> getAllDokter();

    public void insertDokter(Dokter dokter);

    public void updateDokter(Dokter dokter);

    public void deleteDokter(Dokter dokter);
}
