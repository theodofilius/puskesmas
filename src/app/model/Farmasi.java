/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

/**
 *
 * @author theodofilius
 */
public class Farmasi {

    private int obatID;
    private int dokterID;
    private int pasienID;
    private int labID;
    private String namaobat;
    private String pemakaian;
    
//    Setter
    public void setObatId(int obatID) {
        this.obatID = obatID;
    }

    public void setDokterId(int dokterID) {
        this.dokterID = dokterID;
    }

    public void setPasienId(int pasienID) {
        this.pasienID = pasienID;
    }

    public void setLabId(int labID) {
        this.labID = labID;
    }

    public void setNamaObat(String namaobat) {
        this.namaobat = namaobat;
    }

    public void setPemakaian(String pemakaian) {
        this.pemakaian = pemakaian;
    }

//    Getter
    public int getObatId() {
        return obatID;
    }

    public String getNamaObat() {
        return namaobat;
    }

    public String getPemakaian() {
        return pemakaian;
    }

    public Integer getDokterId() {
        return dokterID;
    }

    public Integer getPasienId() {
        return pasienID;
    }

    public Integer getLabId() {
        return labID;
    }
}
