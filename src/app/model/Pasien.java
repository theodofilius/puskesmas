/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

/**
 *
 * @author theodofilius
 */
public class Pasien {
    private int id;
    private String nama;
    private int umur;
    private String tgl;
    private String alamat;
    
    //Set Model Pasien
    public void setID(Integer id){
        this.id=id;
    }
    public void setNama(String nama){
        this.nama=nama;
    }
    public void setUmur(Integer umur){
        this.umur=umur;
    }
    public void setTgl(String tgl){
        this.tgl=tgl;
    }
    public void setAlamat(String alamat){
        this.alamat=alamat;
    }
    
//    Get Model pasien
    public Integer getID(){
        return id;
    }
    public String getNama(){
        return nama;
    }
    public Integer getUmur(){
        return umur;
    }
    public String getTgl(){
        return tgl;
    }
    public String getAlamat(){
        return alamat;
    }
}
