/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

/**
 *
 * @author theodofilius
 */
public class Dokter {

    private int id;
    private String namaD;
    private String spesialisD;

    public void setID(int id) {
        this.id = id;
    }

    public void setNamaD(String namaD) {
        this.namaD = namaD;
    }

    public void setSpesialisD(String spesialisD) {
        this.spesialisD = spesialisD;
    }

    public Integer getID() {
        return id;
    }

    public String getNamaD() {
        return namaD;
    }

    public String getSpesialisD() {
        return spesialisD;
    }
}
