/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

/**
 *
 * @author theodofilius
 */
public class Antrian {

    private int pasienID;
    private int no_antrian;

//    Setter
    public void setPasienID(int pasienID) {
        this.pasienID = pasienID;
    }

    public void setNoAntrian(int no_antrian) {
        this.no_antrian = no_antrian;
    }

//    Getter
    public Integer getPasienID() {
        return pasienID;
    }

    public Integer getNoAntrian() {
        return no_antrian;
    }
}
